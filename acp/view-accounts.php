<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-users-cog"></i> View accounts</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-dark" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>User Rank</th>
                            <th>Join Date</th>
                            <th>Last IP</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>User Rank</th>
                            <th>Join Date</th>
                            <th>Last IP</th>
                            <th>Tools</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $accounts_query = $mysqliA->query("SELECT * FROM `account`") or die (mysqli_error($mysqliA));
                        $num_query = $accounts_query->num_rows;
                        if($num_query < 1)
                        {
                            echo '<tr><td colspan="6">There are no accounts!</td></tr>';
                        }
                        else
                        {
                            while($res = $accounts_query->fetch_assoc())
                            {
                                $accountID = $res['id'];
                                $username = $res['username'];
                                $email = $res['email'];
                                $joindate = $res['joindate'];
                                $lastIP = $res['last_ip'];
                                $rank_query = $mysqliA->query("SELECT * FROM `account_access` WHERE `id` = '$accountID'") or die (mysqli_error($mysqliA));
                                $num_rank = $rank_query->num_rows;
                                if($num_rank < 1)
                                {
                                    $class = 'success';
                                    $rank = 'Standard User';
                                }
                                else
                                {
                                    while($rank_res = $rank_query->fetch_assoc())
                                    {
                                        $rank_result = $rank_res['gmlevel'];

                                        switch ($rank_result) {
                                            case "3":
                                                $class = 'danger';
                                                break;
                                            case "2":
                                                $class = 'warning';
                                                break;
                                            case "1":
                                                $class = 'primary';
                                                break;
                                        }

                                        switch ($rank_result) {
                                            case "3":
                                                $rank = 'Administrator';
                                                break;
                                            case "2":
                                                $rank = 'Game Master';
                                                break;
                                            case "1":
                                                $rank = 'Moderator';
                                                break;
                                        }
                                    }
                                }


                                echo '
                                    <tr>
                                       <td>'. $username .'</td>
                                       <td>'. $email .'</td>
                                       <td><span class="badge badge-'. $class .'">'. $rank .'</span></td>
                                       <td>'. $joindate .'</td>
                                       <td>'. $lastIP .'</td>
                                       <td><a href="'.$custdir.'/acp/account-details.php?id='. $accountID .'" class="btn btn-sm btn-primary"><i class="fad fa-file-user"></i> Account details</a></td>
                                    </tr>
                                    ';
                            }
                        }

                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>