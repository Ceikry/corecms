<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title">Welcome to <?php echo $site_name; ?>!</h2>
                        <p class="card-text">
                            Please feel free to browse our website!<br />
                            If you need assistance please go to our <strong>help</strong> page where you can find more information that might be useful for you!
                        </p>
                    </div>
                </div>
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title">Online Players <?php if ($multirealm == 1) { echo '(BFA)';} ?></h2>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Race</th>
                                <th scope="col">Class</th>
                                <th scope="col">Level</th>
                            </tr>
                            </thead>
                            <tbody>
							<?php
							//online players
                            $get_online = $mysqliC->query("SELECT * FROM `characters` WHERE `online` = '1';") or die (mysqli_error($mysqliC));
							$num_online = $get_online->num_rows;
							if($num_online < 1)
							{
								echo '<tr><td colspan="4">Realm is offline or there are no players online!</td></tr>';
							}
							else
							{
								while($online = $get_online->fetch_assoc())
								{
									echo '
										<tr>
											<td class="class-'. $online['class'] .'">'. $online['name'] .'</td>
											<td><img src="images/races/'. $online['race'] .'_'. $online['gender'] .'.png" width="16" height="16" alt="race"></td>
											<td><img src="images/classes/'. $online['class'] .'.png" width="16" height="16" alt="class"></td>
											<td>'. $online['level'] .'</td>
										</tr>
									';
								}
							}
							?>
                            </tbody>
                        </table>

                    </div>
                    <div class="card-footer text-muted text-info">
                    <!-- <i class="fad fa-globe-americas"></i> Maximum online players: <strong>SOON</strong> -->
                    </div>
                </div>
                
                <?php if ($multirealm == 1) {?>
                    <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title">Online Players (SL)</h2>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Race</th>
                                <th scope="col">Class</th>
                                <th scope="col">Level</th>
                            </tr>
                            </thead>
                            <tbody>
							<?php
							//online players
                            $get_online = $mysqliSC->query("SELECT * FROM `characters` WHERE `online` = '1';") or die (mysqli_error($mysqliSC));
							$num_online = $get_online->num_rows;
							if($num_online < 1)
							{
								echo '<tr><td colspan="4">Realm is offline or there are no players online!</td></tr>';
							}
							else
							{
								while($online = $get_online->fetch_assoc())
								{
									echo '
										<tr>
											<td class="class-'. $online['class'] .'">'. $online['name'] .'</td>
											<td><img src="images/races/'. $online['race'] .'_'. $online['gender'] .'.png" width="16" height="16" alt="race"></td>
											<td><img src="images/classes/'. $online['class'] .'.png" width="16" height="16" alt="class"></td>
											<td>'. $online['level'] .'</td>
										</tr>
									';
								}
							}
							?>
                            </tbody>
                        </table>

                    </div>
                    <div class="card-footer text-muted text-info">
                    <!-- <i class="fad fa-globe-americas"></i> Maximum online players: <strong>SOON</strong> -->
                    </div>
                </div>
                <?php  }?>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>